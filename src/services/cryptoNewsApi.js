 import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
const BASE_URL = 'https://bing-news-search1.p.rapidapi.com';
const CRYPTO_API_HEADERS = {
    'X-BingApis-SDK': true,
'X-RapidAPI-Host': process.env.REACT_APP_NEWS_API_X_RapidAPI_Host,
    'X-RapidAPI-Key': process.env.REACT_APP_NEWS_API_X_RapidAPI_Key
}
export const   cryptoNewsApi  = createApi({
    reducerPath: 'cryptoNewsApi',
    baseQuery:fetchBaseQuery({
        baseUrl: BASE_URL,
        headers:CRYPTO_API_HEADERS
    }),
    tagTypes:['news'],
    endpoints: (builder)=>({
        getCryptoNews: builder.query({
              query:(newsCategory )=>({
                  url:`/news/search?q=${newsCategory}&safeSearch=Strict&freshness=Day&textDecorations:true&textFormat=Raw`,
                  headers:CRYPTO_API_HEADERS
              }),
        })
    })
});
 
export default cryptoNewsApi.reducer;
export const {useGetCryptoNewsQuery} = cryptoNewsApi;