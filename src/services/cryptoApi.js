 
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

const BASE_URL = 'https://coinranking1.p.rapidapi.com';

const CRYPTO_API_HEADERS = {
    'X-RapidAPI-Host': process.env.REACT_APP_CRYPTO_API_X_RapidAPI_Host,
    'X-RapidAPI-Key': process.env.REACT_APP_CRYPTO_API_X_RapidAPI_Key
} 
export const   cryptoApi  = createApi({
    reducerPath: 'cryptoApi',
    baseQuery:fetchBaseQuery({
        baseUrl: BASE_URL,
        headers:CRYPTO_API_HEADERS
    }),
    tagTypes:['Coins'],
    endpoints: (builder)=>({
        getCoins: builder.query({
              query:(limit)=>({
                  url:`/coins?limit=${limit}`,
                  headers:CRYPTO_API_HEADERS
              }),
        }),
        getCoinDetail: builder.query({
            query:(coinId)=>({
                url:`/coin/${coinId}`,
                headers:CRYPTO_API_HEADERS
            }),
      }),
 
      getCryptoHistory: builder.query({
        query: ({ coinId, timePeriod }) => ({
            url:`coin/${coinId}/history?timePeriod=${timePeriod}`,
            headers:CRYPTO_API_HEADERS
        }),
      }),
   
    })
});
 

export default cryptoApi.reducer;
export const {useGetCoinsQuery,useGetCoinDetailQuery, useGetCryptoHistoryQuery} = cryptoApi;