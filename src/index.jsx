import React from "react";
import ReactDom from 'react-dom/client';
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import { Provider } from "react-redux";
import { store } from "./app/store";

const root = ReactDom.createRoot(document.querySelector("#root"));

root.render(
    <React.StrictMode>
        <Provider store={store}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </Provider>
    </React.StrictMode>
);