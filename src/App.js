import {  Space, Typography } from 'antd';
import { Layout } from 'antd';
import React from 'react';
import { Link, Route, Routes } from "react-router-dom";
import './App.css';

import { Navbar , HomePage, Cyptocurrencies, CyptocurrencyDetail, News } from './components';



const App = () => {
    return (
        <div className="app">
            <nav className="navbar">
                <Navbar />
            </nav>
            <main className="main">
                <Layout>
                    <div className="routes">
                        <Routes>
                            <Route path="/" index element={<HomePage />}  />
                            <Route path="/cyptocurrencies" element={<Cyptocurrencies />} />
                            <Route path="/cyptocurrencies/:coinId" element={<CyptocurrencyDetail />} />
                            <Route path="/news" element={<News />} />
                             
                        </Routes>
                    </div>
                </Layout>
                <footer className="footer">
                <Typography.Title level={5} style={{fontSize:"1rem", color:"#fff"}}>
                    Crypto World
                    <br /> © 2020 Created by  Jaswant Bisht 
                </Typography.Title>
                <Space>
                    <Link to='/home'>Home</Link>
                     <Link to='/cyptocurrencies'>Cyptocurrencies</Link>
                    <Link to='/news'>News</Link>
                </Space>
            </footer>
            </main>
       
        </div>
    );
};

export default App;