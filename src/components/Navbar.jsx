import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Radio, Menu, Typography, Image, Avatar } from 'antd';
import { HomeOutlined, MoneyCollectOutlined, BulbOutlined, MenuOutlined, FundOutlined } from '@ant-design/icons';
import Logo from '../images/dev.jpg'
import { useState, useEffect } from 'react';



const Navbar = () => {

    const [activeMenu, setActiveMenu] = useState(true);
    const [screenSize, setScreenSize] = useState(null);

    useEffect(() => {
        const resize = () => setScreenSize(window.innerWidth);

        window.addEventListener('resize', resize);

        return () => {
            window.removeEventListener('resize', resize);
        }
    }, []);


    useEffect(() => {
        if (screenSize < 768) {
            setActiveMenu(false);
        } else {
            setActiveMenu(true);
        }
    }, [screenSize]);

    return (
        <div className='nav-container'>
            <div className="logo-container">
                <Avatar src={<Image src={Logo} size="large" />} />
                <Typography.Title level={4} className='logo'>
                    <Link to='/'>Crypto World </Link>
                </Typography.Title>
                <Button onClick={()=>setActiveMenu(!activeMenu)} className='menu-control-container'> <MenuOutlined/> </Button>
            </div>
            {
                activeMenu && <Menu theme='dark' >
                    <Menu.Item key='home' icon={<HomeOutlined />}> <Link to='/'>Home</Link>  </Menu.Item>
                    <Menu.Item key='Cyptocurrencies' icon={<FundOutlined />}> <Link to='/cyptocurrencies'>Cyptocurrencies</Link>  </Menu.Item>
                    <Menu.Item key='news' icon={<BulbOutlined />}> <Link to='/news'>News</Link>  </Menu.Item>
                </Menu>
            }
        </div>
    );
};

export default Navbar;