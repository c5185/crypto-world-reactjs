import React, { useState } from 'react';
import { Typography } from "antd";
import { Statistic, Row, Col, Button } from 'antd';
import { useGetCoinsQuery } from '../services/cryptoApi';
import millify from 'millify'
import {Link} from 'react-router-dom';
import Cyptocurrencies from './Cyptocurrencies';
import News from './News';

const HomePage = () => {
    const { data, isError, isLoading } = useGetCoinsQuery(10);
   let globalStats =   data?.data?.stats; 

    if (isLoading) {
        return <Typography.Text>Loading data please wait...</Typography.Text>; 
    }
    return (
        <>
            <Typography.Title level={2} className='heading'>Global Crypto Statitics</Typography.Title>
            <Row gutter={16}>
                <Col span={12}> <Statistic title="Total Crypto Currencies" value={ millify (globalStats?.total) } />  </Col>
                <Col span={12}> <Statistic title="Total Exchanges  " value={ millify (globalStats?.totalExchanges) } />  </Col>
                <Col span={12}> <Statistic title="Total Market Cap" value={ millify (globalStats?.totalMarketCap) } />  </Col>
                <Col span={12}> <Statistic title="Total 24th volume" value={ millify (globalStats?.total24hVolume) } />  </Col>
                <Col span={12}> <Statistic title="Total Market" value={ millify (globalStats?.totalMarkets) } />  </Col>
            </Row>
            <div className="home-heading-container">
                <Typography.Title level={2} className='heading'>Top 10 Cryptocurrencies in the world</Typography.Title>
                <Typography.Paragraph  className='show-more' > <Link to={'/cyptocurrencies'}> Show more</Link> </Typography.Paragraph>
            </div>
            <Cyptocurrencies simplified={true}/>
            <div className="home-heading-container">
                <Typography.Title level={2} className='heading'>Top Crypto News</Typography.Title>
                <Typography.Paragraph  className='show-more'> <Link to={'/news'}> Show more</Link> </Typography.Paragraph>
            </div>
            <News simplified={true}/>
            
        </>
    );
};

export default HomePage;