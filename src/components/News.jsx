import { Badge, Button, Card, Col, Image, Row, Typography, Select, Avatar } from 'antd';
import React, { useEffect, useState } from 'react';
import { useGetCryptoNewsQuery } from '../services/cryptoNewsApi';
import { Link } from 'react-router-dom';
import { useGetCoinsQuery } from '../services/cryptoApi';
import moment from 'moment';
const News = ({ simplified }) => {

    const [news, setNews] = useState([]);
    const { data: coinList } = useGetCoinsQuery(100);
    const [selectedValue, setSelectedValue] = useState('')

    const [coins, setCoins] = useState([]);

    // console.log(useGetCryptoNewsQuery());
    const { data, isLoading, isError } = useGetCryptoNewsQuery(selectedValue);


    useEffect(() => {
        setNews(data?.value)
        setCoins(coinList?.data?.coins);
    }, [data?.value, coinList?.data?.coins, selectedValue]);


    if (isLoading) {
        return <Typography.Text>Loading data please wait...</Typography.Text>;
    }
    return (
        <>



            <Row gutter={[24, 24]} >
                
                {
                    !simplified && <Col span={24}>

                    <Select
                        className='select-news'
                        showSearch
                        filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase())}
                        placeholder="Search Crypto News"
                        onChange={(value) => setSelectedValue(value)}
                    >
                        {
                            coins?.map((coin) => <Select.Option key={coin?.uuid} value={coin?.name}>{coin?.name}</Select.Option>)
                        }

                    </Select>
                </Col>

                }

                {
                    news?.slice(0, simplified ? 5 : 100)?.map((currentNews, index) => {
                        return (<Col key={currentNews?.name} lg={8} sm={12} xs={24} className='crypto-card'>

                            <Card hoverable className='news-card' >
                                <a href={currentNews?.url} target='_blank' rel='noreferrer' >
                                    <div className="news-image-container">
                                        <Image style={{ maxWidth: "200px", maxHeight: "100px" }} src={currentNews?.image?.thumbnail?.contentUrl} />
                                        <Typography.Title className='news-title' level={4}>  {currentNews?.name?.substring(0, simplified ? 45 : currentNews?.name.length)} </Typography.Title>
                                        <Typography.Paragraph> {currentNews?.description} </Typography.Paragraph>

                                        <div className="provider-container">
                                         <div className="">
                                   <Avatar size={32} src={currentNews?.provider[0]?.image?.thumbnail.contentUrl} />
                                            <Typography.Text className='provider-name'> {currentNews?.provider[0]?.name} </Typography.Text> 
                                            </div>     
                                            <Typography.Text> { moment(currentNews?.datePublished).startOf('s').fromNow()} </Typography.Text> 
                                        </div>

                                    </div>
                                </a>

                             
                            </Card>

                        </Col>)
                    })
                }

            </Row>

        </>
    );
};

export default News;