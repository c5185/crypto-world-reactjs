import React from 'react';
import { useGetCoinsQuery } from '../services/cryptoApi';
import { Card, Image, Input, Typography } from "antd";
import { Statistic, Row, Col, Button } from 'antd';
import millify from 'millify'
import { Link } from 'react-router-dom';
import { useState } from 'react';
import { useEffect } from 'react';
 

 
const Cyptocurrencies = ({simplified}) => {
    const count = simplified ? 10 : 100;
    const { data, isError, isLoading } = useGetCoinsQuery(count);
     
    const [coins, setCoins] = useState(data?.data?.coins);
    const [searchTerm, setSearchTerm] = useState('');
    

    useEffect(() => {
       let filteredCoins = data?.data?.coins?.filter(coin => coin.name.toLowerCase().includes(searchTerm.toLowerCase()));
         setCoins(filteredCoins);
    }, [data?.data?.coins, searchTerm]);
    

    if (isLoading) {
        return <Typography.Text>Loading data please wait...</Typography.Text>;
    }

    return (
        <>

        {
            !simplified &&   <div className="search-crypto">
            <Input onChange={(e)=>setSearchTerm(e.target.value)} placeholder='Ssearch crypto currencies............' />
        </div>
        }
         
            <Row gutter={[32, 32]} className='crypto-card-container' >
                {
                    coins?.map((coin, index) => {
                        return (<Col key={coin?.uuid} lg={6} md={12} xs={24} className='crypto-card'>
                            <Link to={`/cyptocurrencies/${coin.uuid}`}>
                                <Card
                                    hoverable
                                    title={`${coin?.rank}, ${coin?.symbol} `}
                                    extra={<Image className='crypto-image' alt={` ${coin?.name}`} src={coin?.iconUrl} />}
                                >

                                    <Typography.Title level={2}>  {coin?.name} </Typography.Title>
                                    <Typography.Paragraph>Price : {millify(coin?.price)}</Typography.Paragraph>
                                    <Typography.Paragraph>marketCap : {millify(coin?.marketCap)}</Typography.Paragraph>
                                    <Typography.Paragraph>Daily change : {millify(coin?.change)}%</Typography.Paragraph>
                                    <Typography.Paragraph>24hVolume : {millify(coin['24hVolume'])}</Typography.Paragraph>
                                </Card>
                            </Link>
                        </Col>)
                    })
                }

            </Row>
        </>
    );
};

export default Cyptocurrencies;