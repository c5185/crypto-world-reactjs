export { default as Navbar } from './Navbar';
export { default as Cyptocurrencies } from './Cyptocurrencies'; 
export { default as News } from './News';
export { default as CyptocurrencyDetail } from './CyptocurrencyDetail';
export { default as HomePage } from './HomePage';
